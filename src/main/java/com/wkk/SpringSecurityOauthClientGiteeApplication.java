package com.wkk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityOauthClientGiteeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityOauthClientGiteeApplication.class, args);
    }

}
